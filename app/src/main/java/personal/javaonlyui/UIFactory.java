package personal.javaonlyui;


import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

class UIFactory {

    static LinearLayout initLinearLayout(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        linearLayout.setGravity(Gravity.CENTER);
        return linearLayout;
    }

    static TextView initTextView(Context context) {
        final float textSize = 30;
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        return textView;
    }

    static EditText initEditText(Context context) {
        final float textSize = 30;
        final float widthInDP = 200;
        final DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,(widthInDP),metrics);
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,(textSize+20),metrics);

        EditText editText = new EditText(context);

        editText.setLayoutParams(new LinearLayout.LayoutParams(width,height));
        editText.setTextSize(textSize);
        return editText;
    }

    static Button initButton(Context context) {
        final float textSize = 40;
        final float widthInDP = 200;
        final DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,(widthInDP),metrics);
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,(textSize+20),metrics);

        Button button = new Button(context);

        button.setLayoutParams(new LinearLayout.LayoutParams(width,height));
        button.setTextSize(textSize);
        return button;
    }
}
