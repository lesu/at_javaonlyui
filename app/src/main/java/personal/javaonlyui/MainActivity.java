package personal.javaonlyui;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{
    private LinearLayout mainLayout;
    private EditText editTextIn;
    private Button button;
    private TextView textViewOut;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this.getApplicationContext();
        this.mainLayout = UIFactory.initLinearLayout(this.context);
        this.setContentView(this.mainLayout);
        this.initComponents();
    }


    private void initComponents() {
        this.editTextIn = UIFactory.initEditText(this.context);
        this.editTextIn.setTextColor(Color.BLACK);
        this.editTextIn.setBackgroundColor(Color.GRAY);
        this.mainLayout.addView(this.editTextIn);

        this.button = UIFactory.initButton(this.context);
        this.button.setTextColor(Color.BLACK);
        this.button.setText("View");
        this.mainLayout.addView(this.button);

        this.textViewOut = UIFactory.initTextView(this.context);
        this.textViewOut.setTextColor(Color.BLACK);
        this.mainLayout.addView(this.textViewOut);

        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = MainActivity.this.editTextIn.getText().toString();
                MainActivity.this.editTextIn.setText("");
                MainActivity.this.textViewOut.setText(text);
            }
        });
    }



}
